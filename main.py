# -*- coding: utf-8 -*-
import telebot, re
TOKEN = ""
admins = [136680745,215429119]
bot = telebot.TeleBot(TOKEN)
@bot.message_handler(content_types=["text"])
def spam(message):
    try:
        if re.match("^[/#!]?spam (.*) (.*)", message.text) and message.from_user.id in admins:
            matches = re.match("^[/#!]?spam (.*) (.*)", message.text).groups()
            id = matches[0]
            for n in range(0, int(matches[1])):
                n+=1
                bot.reply_to(message, "{}".format(id))
        elif re.match("^[/#!]?spam (.*) (.*)", message.text) and message.from_user.id not in admins:
            bot.reply_to(message, "you are not an admin")
    except Exception as e:
        print(e)
        bot.reply_to(message, "*Error !*", parse_mode="markdown")
bot.polling(True)
